#  Angular JS intermediate APP

This simple app is meant to outline some of the key concepts of building
an app in angular js. It outlines:

  - controllers
  - services
  - directives
  - filters
  - routing

### Installation

```sh
$ git clone https://bitbucket.org:codinggrace/angular-js-intermediate.git todoapp
$ cd todoapp
$ npm install
$ npm install -g bower
$ bower install
```

### Running

```sh
$ node app.js
$ open http://localhost:4010/app#/list
```

### Debugging

```sh
$ DEBUG=express:* node app.js
```


### Dependencies

This app uses a number of dependencies

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - Our server of choice for serving static files and persisting resources
* [npm] - Node Package Manager
* [Express] - fast node.js network app framework
* [Bower.io] - Dependency and package manager

### References:
Tutorial: https://www.airpair.com/angularjs/posts/angularjs-tutorial 

[AngularJS]:http://angularjs.org
[Twitter Bootstrap]:http://twitter.github.com/bootstrap/
[node.js]:http://nodejs.org
[npm]:https://www.npmjs.com/
[express]:http://expressjs.com
[Bower.io]:http://bower.io

