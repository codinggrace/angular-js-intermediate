var express = require('express');
var app = express();

app.use(express.static('./public'))

app.use(function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

var server = app.listen(4010, function () {

  var port = server.address().port;
  console.log('TODO app listening at http://localhost:' + port);

});
