(function (angular) {
  'use strict';

  angular
      .module('anotherTodoApp')
      .controller('ItemCreateCtrl', ItemCreateCtrl)
      .controller('ItemListCtrl', ItemListCtrl);

  /*@ngInject*/
  function ItemCreateCtrl($scope, $location, StorageService) {

    $scope.todo = {};
    $scope.now = new Date();


    var todos = StorageService.list() || [];

    $scope.add = function(){

      var todo = {
        name: $scope.todo.name,
        done: $scope.todo.done
      }

      if($scope.todo.done){
        todo.timestamp = new Date();

      }

      todos.push(todo);

      StorageService.save(todos);

      $location.path('/');

    };

  }

  /*@ngInject*/
  function ItemListCtrl($scope, StorageService) {

    $scope.todos = StorageService.list();

    $scope.delete = function(index){

      $scope.todos.splice(index, 1);
      StorageService.save(listName, $scope.todos);

    };

    $scope.getFormat = function(){

      return 'yyyy-MM-dd';

    }
  }

  /*@ngInject*/
  function ACtrl($scope, StorageService) {


  }




})(window.angular);



