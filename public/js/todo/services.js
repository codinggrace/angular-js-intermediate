(function (angular) {
  'use strict';

  angular
      .module('anotherTodoApp')
      .factory('StorageService', StorageService);

  /*@ngInject*/
  function StorageService(localStorageService){

    return {

       save: save,
       list: list

     };


    var KEY = "my-local-storage";

    function save(val){

      return localStorageService.set(KEY, val);

    };


    function list(){

      return localStorageService.get(KEY);

    };

  }


})(window.angular);



