(function (angular) {
  'use strict';

  angular
      .module('anotherTodoApp', ['ngRoute', 'LocalStorageModule'])
      .config(configureRoutes);

  /*@ngInject*/
  function configureRoutes($routeProvider) {
    $routeProvider
        .when('/create', {

          templateUrl: '/js/todo/partials/create.html',
          controller: 'ItemCreateCtrl'

        }).when('/list', {

          templateUrl: '/js/todo/partials/list.html',
          controller: 'ItemListCtrl',
          reloadOnSearch: false

        }).otherwise({
          redirectTo: '/list'
        })
  }

}(window.angular));