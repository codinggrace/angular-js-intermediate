(function(){
	'use strict';

	angular.module('todoApp')
	  .directive('todoCount', ['TodoService', function(TodoService){
	  	return{
	  		scope: {
	  			totalTodos: '=',
	  			firstTodoName: '@'
	  		},
	  		restrict: 'E',
	  		templateUrl: '/app/directives/todoCount/todoCount.html',
	  		link: function(scope, el, attrs){
	  			// this bit no longer needed, number of todos passed as attribue on
					// directive's body
	  			//scope.totalTodos = TodoService.todos.length;
	  		}
	  	};
	  }]);


})();
