(function(){
	'use strict';

	angular.module('todoApp')
	  .controller('EditTodoController', ['$scope', '$location', '$routeParams', 'TodoService', EditTodoController]);

	  function EditTodoController($scope, $location, $routeParams, TodoService){

	  	console.log('loaded edit event controller' + $routeParams.todoId);

	  	var id = $routeParams.todoId;

	  	$scope.todo = TodoService.GetTodoById(id);

	  	$scope.editTodo = function(){
	  		TodoService.todos[id] = {
	  			name: $scope.todo.name,
	  			done: $scope.todo.done
	  		};

	  		$location.path('/');
	  	}

	  }
})();