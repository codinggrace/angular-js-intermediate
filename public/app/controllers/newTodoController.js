(function(){
	'use strict';

	angular.module('todoApp')
	  .controller('NewTodoController', ['$scope', '$location', 'TodoService', NewTodoController]);

	  function NewTodoController($scope, $location, TodoService){

	  	$scope.todo = {};

	  	$scope.addTodo = function(){
	  		
	  		TodoService.todos.push({
	  			name: $scope.todo.name,
	  			done: $scope.todo.done
	  		});

	  		console.log('last added todo: ', TodoService.todos[TodoService.todos.length-1]);

	  		$location.path('/');
	  	}

	  }
})();