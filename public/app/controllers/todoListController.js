(function(){
	'use strict';

	angular.module('todoApp')
	  .controller('TodoListController', ['$scope', 'TodoService', TodoListController]);

	  function TodoListController($scope, TodoService){

	  	$scope.todos = TodoService.todos;

	  	$scope.delete = function(i){
	  		//todo removed from the controller
	  		$scope.todos.splice(i, 1);
	  		//update the todos service
	  	}
	  }

})();
