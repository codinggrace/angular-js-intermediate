(function(){
	'use strict';

	angular.module('todoApp')
	  .service('TodoService', [TodoService])

	  function TodoService(){
	  	
	  	this.sampleServiceValue = 'I am in factory'

	  	this.todos = [
	  		{
	  			name: 'Clean the desk',
	  			done: false 
	  		},
	  		{
	  			name: 'Call Mark',
	  			done: false 
	  		},
	  		{
	  			name: 'Attend the workshops',
	  			done: true 
	  		},
	  		{
	  			name: 'Learn Angular',
	  			done: false 
	  		},
	  	];
	  	
	  	this.GetTodoById = function(id){
	  		return this.todos[id];
	  	};
	  }
})();