(function(){
	'use strict';

	angular.module('todoApp')
	  .factory('TodoFactory', [TodoFactory])

	  function TodoFactory(){
	  	
	  	return {
	  		sampleFactoryValue: 'I am in factory'
	  	};

	  }
})();