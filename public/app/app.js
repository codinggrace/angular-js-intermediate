(function (angular) {
  'use strict';

angular.module('todoApp', ['ngRoute'])
  .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

  	$locationProvider.html5Mode(true);

		$routeProvider
			.when('/', {
				templateUrl: '/app/views/todoList.html',
				controller: 'TodoListController'
			})
			.when('/new', {
				templateUrl: '/app/views/newTodo.html',
				controller: 'NewTodoController'
			})
			.when('/edit/:todoId', {
				templateUrl: '/app/views/editTodo.html',
				controller: 'EditTodoController'
			})
			.otherwise({
				redirectTo: '/'
			});
  }]);
})(window.angular);
